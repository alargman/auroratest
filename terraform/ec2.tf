provider "aws" {
  region = var.aws_region
}

resource "aws_instance" "my_webserver" {
  ami                           = var.ami_id
  instance_type                 = var.instance_type
  vpc_security_group_ids        = [aws_security_group.web-servers.id]
  key_name                      = aws_key_pair.public-node.key_name
  associate_public_ip_address   = true
  user_data                     = file("user_data.sh")

  tags = {
    Name  = "Aurora Server"
    Owner = "Alex Largman"
  }
}

