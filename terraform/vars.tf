variable "aws_region" {
  default     = "us-east-1"
  description = "aws region where our resources are going to create choose"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "ami_id" {
  default = "ami-09e67e426f25ce0d7"
}

variable "ssh-public-node" {
  type = map(string)
  default = {
    "PATH_TO_PRIVATE_KEY" = "~/.ssh/id_rsa/public-node"
    "PATH_TO_PUBLIC_KEY" = "~/.ssh/id_rsa/public-node.pub"
  }
}

variable "users" {
  type = map(string)
  default = {
    "ubuntu" = "ubuntu"
  }
}
