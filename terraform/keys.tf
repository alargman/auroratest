resource "aws_key_pair" "public-node" {
  key_name   = "public-node"
  public_key = file(var.ssh-public-node["PATH_TO_PUBLIC_KEY"])
}