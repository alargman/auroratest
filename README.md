# AuroraTest
This project creates EC2 t2.micro Ubuntu instance with Docker aboard with necessary security groups in default VPC by means of IaaC Terraform and demonstrates how Jenkins pipelines work.

## How to do
You should have terraform on board and AWS credentials to get access to your AWS account.

### 1. Clone repository and start the Terraform script
```
git clone https://gitlab.com/alargman/auroratest.git
```
```
cd WaveProject/auroratest/terraform
```

### 2. Generate ssh key and call it public-node
```
ssh-keygen
```

### 3. If you don't have Terraform, install it. If you have it, then export AWS credentials and default region from your AWS account
```
export AWS_ACCESS_KEY_ID=xxxxxxxxxxxxxxx
export AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxx
export AWS_DEFAULT_REGION=us-east-1
```

### 4. Start your EC2 instance
```
terraform init
```
```
terraform plan
```
```
terraform apply -auto-approve
```

### 5. Then you can run your Jenkins with the Docker image
~~~
mkdir jenkins
sudo docker run --name myjenkins -p 8080:8080 -p 50000:50000 -v ~/jenkins:/var/jenkins_home jenkins/jenkins:lts-jdk11
~~~
Jenkins is available by url http://YouServerIP:8080

### 6. When you finish with Jenkins you can destroy the system:
```
terraform destroy -auto-approve
```
